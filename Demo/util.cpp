//
// util.cpp
// ���ߺ�����
//

#include "stdafx.h"
#include "util.h"

SysLanguage GetSysLang()
{
	long langID = 0;

	langID = GetSystemDefaultLangID();

	switch (langID)
	{
	case PRC:
		return SimpChs;
		break;
	case TW:
	case HK:
	case SGP:
		return TradChs;
		break;
	default:
		return English;
	}
}

BOOL IsFileExist(CString filePath)
{
	if ( filePath.IsEmpty() )
		return false;
	CFileStatus status;
	return CFile::GetStatus( filePath, status);
}