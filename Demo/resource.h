//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Demo.rc
//
#define IDD_DEMO_DIALOG                 102
#define IDR_MAINFRAME                   133
#define IDC_EXPLORER                    1000
#define IDC_STORE_URL                   1001
#define IDC_IE_STATUS                   1002
#define IDC_CUSTOM_STATUS               1003
#define IDC_PROGRESS1                   1004
#define IDC_PROGRESS                    1004

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        134
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1005
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
