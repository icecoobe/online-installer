// MiniStatic.cpp : 实现文件
//

#include "stdafx.h"
#include "MiniStatic.h"


// CMiniStatic

IMPLEMENT_DYNAMIC(CMiniStatic, CStatic)

CMiniStatic::CMiniStatic()
{

}

CMiniStatic::~CMiniStatic()
{
}


BEGIN_MESSAGE_MAP(CMiniStatic, CStatic)
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()



// CMiniStatic 消息处理程序

void CMiniStatic::OnPaint()
{
	return;
}

BOOL CMiniStatic::OnEraseBkgnd(CDC* pDC)
{
	return true;
}
