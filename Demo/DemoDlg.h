// DemoDlg.h : 头文件
//

#pragma once
#include "explorer.h"
#include "afxcmn.h"
#include "afxwin.h"

#include "resource.h"

#include "MiniStatic.h"

// CDemoDlg 对话框
class CDemoDlg : public CDialog
{
// 构造
public:
	CDemoDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_DEMO_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg LRESULT OnNcHitTest(CPoint point);
	// afx_msg void OnTimer(UINT_PTR nIDEvent);
	DECLARE_MESSAGE_MAP()
public:
	CExplorer myBrowser;
	
	void Start(); // start to download
	void WorkerThreadProc();
	void ProgressUpdate ( LPCTSTR szIEMsg, LPCTSTR szCustomMsg,
                          const int nPercentDone );
	void DisplayStatus( LPCTSTR szCustomMsg );
	CProgressCtrl m_progress;
	CStatic m_stIEMsg;
	CMiniStatic m_stCustomMsg;
	// 存储路径
	CStatic m_StoreURL;
	afx_msg void OnBnClickedCancel();
	// The Cancel Button
	CButton m_btnCancel;
};

static UINT gThreadProc ( void* pv );
static int  Install(CString path);