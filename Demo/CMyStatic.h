#pragma once

class CMyStatic : public CStatic
{
	DECLARE_DYNAMIC(CMyStatic)
protected:
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
};