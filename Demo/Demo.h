// Demo.h : PROJECT_NAME 应用程序的主头文件
//

#pragma once

#ifndef __AFXWIN_H__
	#error "在包含此文件之前包含“stdafx.h”以生成 PCH 文件"
#endif

#include "resource.h"		// 主符号
#include "IdispImp.h"
#include "util.h"

#define DOWNLOAD_URL "http://www.heyibao.com/download/windows32" // 下载链接URL
#define BG_URL		 "http://www.miniyun.cn/statics/bg/miniyun.html" // 背景URL

// CDemoApp:
// 有关此类的实现，请参阅 Demo.cpp
//

class CDemoApp : public CWinApp
{
public:
	CDemoApp();

	// 显示滚动条
	bool m_ScrollBar;
	SysLanguage gSysLang;
	class CImpIDispatch* m_pDispOM;	

// 重写
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

// 实现

	DECLARE_MESSAGE_MAP()
};

extern CDemoApp theApp;
extern LONG g_fAbortDownload;

//获取下载速度的字符串   
CString GetFileTranSpeed(DWORD size,DWORD time);