#pragma once


// CMiniStatic

class CMiniStatic : public CStatic
{
	DECLARE_DYNAMIC(CMiniStatic)

public:
	CMiniStatic();
	virtual ~CMiniStatic();

protected:
	void afx_msg OnPaint();
	BOOL afx_msg OnEraseBkgnd(CDC* pDC);
	DECLARE_MESSAGE_MAP()
};


