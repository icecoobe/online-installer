#ifndef _UTIL_H_
#define _UTIL_H_

#define PRC 0x0804  // 简体

// 繁体中文
#define TW  0x0404	// 台湾
#define HK  0x0c04	// 香港
#define SGP 0x1004  // 新加坡 

// 其余皆英文

enum SysLanguage { SimpChs, TradChs, English};

SysLanguage GetSysLang();
BOOL IsFileExist(CString filePath);
#endif // _UTIL_H_