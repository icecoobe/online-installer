// Demo.cpp : 定义应用程序的类行为。
//

#include "stdafx.h"
#include "Demo.h"
#include "DemoDlg.h"
#include "CustSite.h" 

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CDemoApp

BEGIN_MESSAGE_MAP(CDemoApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CDemoApp 构造

CDemoApp::CDemoApp()
{
	// TODO: 在此处添加构造代码，
	// 将所有重要的初始化放置在 InitInstance 中
}


// 唯一的一个 CDemoApp 对象

CDemoApp theApp;
LONG g_fAbortDownload;

// CDemoApp 初始化

BOOL CDemoApp::InitInstance()
{
	// 如果一个运行在 Windows XP 上的应用程序清单指定要
	// 使用 ComCtl32.dll 版本 6 或更高版本来启用可视化方式，
	//则需要 InitCommonControlsEx()。否则，将无法创建窗口。
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 将它设置为包括所有要在应用程序中使用的
	// 公共控件类。
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();
	
	// ** chencai added ( begin )

	gSysLang = GetSysLang(); // 获取系统语言版本
	this->m_ScrollBar = false;
	CCustomOccManager *pMgr = new CCustomOccManager;

	// Create an IDispatch class for extending the Dynamic HTML Object Model 
	m_pDispOM = new CImpIDispatch;

	// Set our control containment up but using our control container 
	// management class instead of MFC's default
	AfxEnableControlContainer(pMgr);

	// AfxEnableControlContainer();
	// ** chencai added ( End )

	// 标准初始化
	// 如果未使用这些功能并希望减小
	// 最终可执行文件的大小，则应移除下列
	// 不需要的特定初始化例程
	// 更改用于存储设置的注册表项
	// TODO: 应适当修改该字符串，
	// 例如修改为公司或组织名
	SetRegistryKey(_T("Installer Application"));

	CDemoDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: 在此放置处理何时用
		//  “确定”来关闭对话框的代码
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: 在此放置处理何时用
		//  “取消”来关闭对话框的代码
	}

	// 由于对话框已关闭，所以将返回 FALSE 以便退出应用程序，
	//  而不是启动应用程序的消息泵。
	return FALSE;
}

int CDemoApp::ExitInstance() 
{
	if (m_pDispOM)	
		delete m_pDispOM;

	return 0;
}

//获取下载速度的字符串   
CString GetFileTranSpeed(DWORD size,DWORD time)  
{  
    CString _speed;  
    //判断时间是否为0   
    if (time>0){  
        if (size/1024*1000.0/time<1024)  
        {  
                _speed.Format(TEXT("%.2lfKB/s"), size/1024*1000.0/time);  
        }
		else   
        {
                _speed.Format(TEXT("%.2lfMB/s"), (size/1024)*1000.0/time);  
        }
    }
	else  
    {  
        return _speed = "0KB/s";
    }  

    return _speed;  
}  
