// DemoDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "DemoDlg.h"

#include <atltime.h>

#include "Demo.h"
#include "BindStatusCallback.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CString   g_strURL, g_strFile; // 下载链接URL && 存储路径

// CDemoDlg 对话框

CDemoDlg::CDemoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDemoDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CDemoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EXPLORER, myBrowser);
	DDX_Control(pDX, IDC_PROGRESS, m_progress);
	DDX_Control(pDX, IDC_IE_STATUS, m_stIEMsg);
	DDX_Control(pDX, IDC_CUSTOM_STATUS, m_stCustomMsg);
	DDX_Control(pDX, IDC_STORE_URL, m_StoreURL);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
}

BEGIN_MESSAGE_MAP(CDemoDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_WM_NCHITTEST()
	ON_BN_CLICKED(IDCANCEL, &CDemoDlg::OnBnClickedCancel)
	// ON_WM_TIMER()
END_MESSAGE_MAP()


// CDemoDlg 消息处理程序

BOOL CDemoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// 设置窗口标题
	CString szTitle;
	switch (theApp.gSysLang)
	{
	case SimpChs:
		 szTitle = _T("在线安装程序");
		 break;
	case TradChs:
		 szTitle = _T("在線安裝程序");
		 break;
	default:
		 szTitle = _T("The Online Installer");
		 break;
	}
	this->SetWindowText(szTitle);

	// 设置取消按钮Caption
	switch (theApp.gSysLang)
	{
	case SimpChs:
		 this->m_btnCancel.SetWindowText( _T("取消") );
		 break;
	case TradChs:
		 this->m_btnCancel.SetWindowText( _T("取消") );
		 break;
	default:
		 this->m_btnCancel.SetWindowText( _T("Cancel") );
		 break;
	}

	// 进度条初始化
	//m_progress.SetBarColor(RGB(0,0,255)); // 进度条颜色
	m_progress.SetRange( 0, 100 );

	// 图片
	myBrowser.Navigate( _T( BG_URL ), NULL, NULL, NULL, NULL );

	g_strURL = CString( DOWNLOAD_URL );

	TCHAR tempDir[_MAX_PATH];
	GetTempPath( _MAX_PATH, tempDir );
	g_strFile.Empty();
	g_strFile.Append(tempDir);
	g_strFile.Append(_T("\\heyibao-windows32.msi"));

	// m_timerID1 = this->SetTimer( 1, 1000, NULL ); // 安装定时器
	
	this->Start(); // 开始下载

	return TRUE; // 除非将焦点设置到控件，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CDemoDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CDemoDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


LRESULT CDemoDlg::OnNcHitTest(CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	int ret = CDialog::OnNcHitTest(point);

	// if语句的前两行用来禁止改变大小的; 最后一行用来禁止移动的
	if ( HTTOP == ret || HTBOTTOM == ret || HTLEFT == ret || HTRIGHT == ret ) {
			// || HTBOTTOMLEFT == ret || HTBOTTOMRIGHT == ret || HTTOPLEFT == ret || HTTOPRIGHT == ret)
		return HTCLIENT;
	}

	return ret;
}

// start to download
void CDemoDlg::Start() 
{
	CWinThread* pWorkerThread;

    UpdateData();

    // Create our worker thread.  It won't start until we disable all of the
    // dialog controls.

    pWorkerThread = AfxBeginThread ( gThreadProc, this,
                                     THREAD_PRIORITY_NORMAL, 0, 
                                     CREATE_SUSPENDED );

    // Note: it's BAD to pass a CWnd object between threads.
    // it turns out that I got lucky and it works.  All the stuff the worker thread does
    // involves just sending messages to window handles.  Since accessing the
    // data member CWnd::m_hWnd is safe to do across threads, nothing ever
    // asserted to alert me of this mistake.
    if ( NULL != pWorkerThread )
    {
        g_fAbortDownload = 0;

        // Kick off the download!
        pWorkerThread->ResumeThread();
    }
    else
    {
		switch (theApp.gSysLang)
		{
		case SimpChs:
			 AfxMessageBox ( _T("不能创建工作线程！"), MB_ICONERROR );
			 break;
		case TradChs:
			 AfxMessageBox ( _T("不能創建工作線程！"), MB_ICONERROR );
			 break;
		default:
			 AfxMessageBox ( _T("Couldn't create worker thread!"), MB_ICONERROR );
			 break;
		}
    }
}

int  Install(CString filePath)
{
	int ret = 0;
	ShellExecute( NULL, NULL, filePath, _T(" /quiet"), NULL, SW_SHOW );
	return ret;
}

void CDemoDlg::WorkerThreadProc()
{
	CCallback callback;
	HRESULT   hr;

    callback.m_pDlg = this;
	
	m_StoreURL.SetWindowText(g_strFile);

    hr = URLDownloadToFile ( NULL,			// ptr to ActiveX container
                             g_strURL,      // URL to get
                             g_strFile,     // file to store data in
                             0,				// reserved
                             &callback		// ptr to IBindStatusCallback
                           );

    if ( SUCCEEDED(hr) )
    {
		// Download completed successfully, then install the msi package
		Install(g_strFile);
		// 解除定时器
		// this->KillTimer(m_timerID1);
		this->OnCancel(); // 退出程序
    }
    else
    {
        LPTSTR lpszErrorMessage;
        CString sMsg;

        if ( FormatMessage ( FORMAT_MESSAGE_ALLOCATE_BUFFER | 
                              FORMAT_MESSAGE_FROM_SYSTEM | 
                              FORMAT_MESSAGE_IGNORE_INSERTS,
                            NULL, hr, 
                            MAKELANGID ( LANG_NEUTRAL, SUBLANG_DEFAULT ),
                            (LPTSTR) &lpszErrorMessage, 0, NULL ))
        {
			switch (theApp.gSysLang)
			{
			case SimpChs:
				 sMsg.Format ( _T("下载失败。错误号码 = 0x%08lX\n\n%s"),
                          (DWORD) hr, lpszErrorMessage );
				 break;
			case TradChs:
				 sMsg.Format ( _T("下載失敗。錯誤號碼 = 0x%08lX\n\n%s"),
                          (DWORD) hr, lpszErrorMessage );
				 break;
			default:
				 sMsg.Format ( _T("Download failed.  Error = 0x%08lX\n\n%s"),
                          (DWORD) hr, lpszErrorMessage );
				 break;
			}
            LocalFree ( lpszErrorMessage );
        }
        else
        {
			switch (theApp.gSysLang)
			{
			case SimpChs:
				 sMsg.Format ( _T("下载失败。错误号码 = 0x%08lX\n\n没有可用信息。"),
                          (DWORD) hr );
				 break;
			case TradChs:
				 sMsg.Format ( _T("下載失敗。錯誤號碼 = 0x%08lX\n\n沒有可用信息。"),
                          (DWORD) hr );
				 break;
			default:
				 sMsg.Format ( _T("Download failed.  Error = 0x%08lX\n\nNo message available."),
                          (DWORD) hr );
				 break;
			}
        }
		m_stCustomMsg.SetWindowText ( sMsg );
    }
}

void CDemoDlg::ProgressUpdate ( LPCTSTR szIEMsg, LPCTSTR szCustomMsg,
                          const int nPercentDone )
{
	// ASSERT ( AfxIsValidString ( szIEMsg ));
    ASSERT ( AfxIsValidString ( szCustomMsg ));
    ASSERT ( nPercentDone >= 0  &&  nPercentDone <= 100 );

	DisplayStatus( szCustomMsg );
    m_progress.SetPos ( nPercentDone );
}

// 消除了閃爍的問題
void CDemoDlg::DisplayStatus( LPCTSTR szCustomMsg )
{
	CWnd * pWndShow;
	CDC *  pDCShow;
	pWndShow = GetDlgItem(IDC_CUSTOM_STATUS);  // static text控件ID
	pDCShow  = pWndShow->GetDC();

	CDC  memDC;
	CRect rect;
	CBitmap memBmp, *pOldBmp;
	pWndShow->GetClientRect(rect);
	memDC.CreateCompatibleDC(pDCShow);
	memBmp.CreateCompatibleBitmap(&memDC, rect.Width(), rect.Height());
	pOldBmp = memDC.SelectObject(&memBmp);

	// 更新窗口
	pWndShow->UpdateWindow();

	// 背景
	CBrush bgBrush; // 背景颜色画刷 
	bgBrush.CreateSolidBrush(GetSysColor(COLOR_BTNFACE)); // 获取窗体背景颜色
	memDC.FillRect(rect,&bgBrush); // 设置背景颜色

	//memDC.SetBkColor(GetSysColor(COLOR_BTNFACE));
	memDC.FillSolidRect(rect, GetSysColor(COLOR_BTNFACE));

	// 设置为透明模式
	memDC.SetBkMode(TRANSPARENT);
	memDC.TextOut(0, 0, szCustomMsg);

	// 将内存DC中的内容拷贝到设备DC中
	pDCShow->SetBkColor(GetSysColor(COLOR_BTNFACE));
	pDCShow->BitBlt(0, 0, rect.Width(), rect.Height(), &memDC, 0, 0, SRCCOPY);

	// 更新窗口
	pWndShow->GetWindowRect(rect);
	pWndShow->InvalidateRect( rect, FALSE );

	// 清理
	bgBrush.DeleteObject();
	memDC.SelectObject(pOldBmp);
	memBmp.DeleteObject();
	memDC.DeleteDC();
	pWndShow->ReleaseDC(pDCShow); 
}

void CDemoDlg::OnBnClickedCancel()
{
	// TODO: 在此添加控件通知处理程序代码
	OnCancel();
	
	// 解除定时器
	// this->KillTimer(m_timerID1);

	// 点击取消按钮之后，删除未下载完的文件
	CString cmdParam;
	cmdParam.Format( _T(" /c del %s"), g_strFile );
	ShellExecute( NULL, NULL, _T("cmd.exe"), cmdParam, NULL, SW_HIDE );
}

// gThreadProd(): Function for our worker thread.  It just calls 
// CDemoDlg::WorkerThreadProc().
UINT gThreadProc ( void* pv )
{
	CDemoDlg* pDlg = (CDemoDlg*) pv;
	pDlg->WorkerThreadProc();

    return 0;
}