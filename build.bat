@echo off

echo -------------------------------------------------------------------------
echo		Online-Installer Build Tool
echo -------------------------------------------------------------------------

set IDE_PATH=F:\Program Files\Microsoft Visual Studio 9.0\Common7\IDE
set SLN_PATH=G:\新建文件夹\Installer\Demo
set TOOL_PATH=G:\新建文件夹\Installer\Demo\Tools

echo [Please verify the directories below:]
echo 1. Visual Studio IDE Directory
echo "%IDE_PATH%"
echo 2. Solution Directory
echo "%SLN_PATH%"
echo -------------------------------------------------------------------------

echo ------- Now Building ... ------- 
"%IDE_PATH%\devenv"  "%SLN_PATH%\Demo.sln"  /ReBuild "Release|Win32"


echo ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
echo ------- Building is over. -------

echo ------- Now Packing ... -------
"%TOOL_PATH%\upx" -9 -oInstaller.exe "%SLN_PATH%\Release\Installer.exe"
move /Y Installer.exe "%SLN_PATH%\Release\Installer.exe"
echo ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
echo ------- Packing is over. -------

echo Installer path
echo "%SLN_PATH%\Release\Installer.exe"
echo ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
pause